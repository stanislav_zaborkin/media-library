import { Component, Input, OnInit } from '@angular/core';
import { IItem } from '../../interfaces/item';

@Component({
  selector: 'app-media-item',
  templateUrl: './media-item.component.html',
  styleUrls: ['./media-item.component.scss']
})
export class MediaItemComponent implements OnInit {
  @Input() item: IItem;

  constructor() { }

  ngOnInit() {
  }

}
