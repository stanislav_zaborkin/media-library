import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataService } from '../../services/data.service';
import { IOption } from '../../interfaces/option';
import { ISearch } from '../../interfaces/search';

@Component({
  selector: 'app-custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss']
})
export class CustomSelectComponent implements OnInit {
  @Input() type: string;
  @Output() selectChanged = new EventEmitter();
  areOptionsVisible = false;
  caption = '';
  options: IOption[];

  constructor(public dataService: DataService) { }

  public applyFilter(option: IOption): void {
    switch (this.type) {
      case 'filter':
        this.caption = option.title;
        this.dataService.$searchParams.next({
          ...this.dataService.$searchParams.getValue(),
          type: 'filter',
          filter: option.value,
          selectCaption: option.title
        });
        break;
      case 'sort':
        this.caption = option.title;
        this.dataService.$searchParams.next({
          ...this.dataService.$searchParams.getValue(),
          type: 'sort',
          sort: {
            sortField: option.value,
            sortType: 1
          },
          selectCaption: option.title
        });
        break;
    }
  }

  public showOptions(): void {
    this.areOptionsVisible = !this.areOptionsVisible;
  }

  public isOptionSelected(option: IOption): boolean {
    return option.title === this.caption;
  }

  public onClose(): void {
      this.dataService.isSelectVisible.next(false);
  }

  ngOnInit() {
    this.caption = this.dataService.selectOptions[this.type][0].title;
    this.options = this.dataService.selectOptions[this.type];
    this.dataService.$searchParams.subscribe(($searchParams: ISearch) => {
      if ($searchParams.type === 'clear') {
        this.caption = this.dataService.selectOptions[this.type][0].title;
      }
    });
    this.dataService.isSelectVisible.subscribe((isVisible: boolean) => {
       this.areOptionsVisible = isVisible;
    });
  }

}
