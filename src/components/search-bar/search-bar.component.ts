import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ISearch } from '../../interfaces/search';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  searchValue = '';

  constructor(public dataService: DataService) { }

  public onInputChange(): void {
    this.dataService.$searchParams.next({
      ...this.dataService.$searchParams.getValue(),
      type: 'search',
      searchString: this.searchValue
    });
  }

  ngOnInit() {
    this.dataService.$searchParams.subscribe((params: ISearch) => {
      if (params.type === 'clear') { this.searchValue = ''; }
    });
  }

}
