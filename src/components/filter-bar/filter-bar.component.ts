import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.scss']
})
export class FilterBarComponent implements OnInit {

  constructor(private dataService: DataService) { }

  public isDescending(): boolean {
    return this.dataService.$searchParams.getValue().sort.sortType === 1;
  }

  public clear(): void {
    this.dataService.$searchParams.next({
      ...this.dataService.$searchParams.getValue(),
      type: 'clear'
    });
  }

  public changeSortType(): void {
    const prevState = this.dataService.$searchParams.getValue();
    this.dataService.$searchParams.next({
      ...prevState,
      type: 'sort',
      sort: {
        sortField: prevState.sort.sortField,
        sortType: prevState.sort.sortType * -1
      }
    });
  }

  ngOnInit() {}

}
