import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MediaItemComponent } from '../components/media-item/media-item.component';
import { MediaItemsListComponent } from '../pages/media-items-list/media-items-list.component';
import { FilterBarComponent } from '../components/filter-bar/filter-bar.component';
import { SearchBarComponent } from '../components/search-bar/search-bar.component';
import { CustomSelectComponent } from '../components/custom-select/custom-select.component';
import { DataService } from '../services/data.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MediaItemComponent,
    MediaItemsListComponent,
    FilterBarComponent,
    SearchBarComponent,
    CustomSelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
