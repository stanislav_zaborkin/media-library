import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MediaItemsListComponent } from '../pages/media-items-list/media-items-list.component';

const routes: Routes = [
  {
    path: '**', component: MediaItemsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
