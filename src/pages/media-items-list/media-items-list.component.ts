import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { IItem } from '../../interfaces/item';
import { ISearch } from '../../interfaces/search';

@Component({
  selector: 'app-media-items-list',
  templateUrl: './media-items-list.component.html',
  styleUrls: ['./media-items-list.component.scss']
})
export class MediaItemsListComponent implements OnInit {
  items: IItem[] = [];
  displayingItems: IItem[] = [];

  constructor(private dataService: DataService) {}

  private initSort(): void {
    this.dataService.$searchParams.next({
      ...this.dataService.searchInit,
      type: 'sort',
    });
  }

  private sortItems(sortField: string, $searchParams: ISearch): IItem[] {
    return this.displayingItems.sort((a: IItem, b: IItem) => {
      if (a[sortField] > b[sortField]) {
        return $searchParams.sort.sortType === 1 ? 1 : -1;
      }
      if (a[sortField] < b[sortField]) {
        return $searchParams.sort.sortType === 1 ? -1 : 1;
      }
      return 0;
    });
  }

  private filterItems(filter: string): IItem[] {
    return this.items.filter((item: IItem) => item.type === filter);
  }

  private searchItems($searchParams: ISearch): void {
    const sortField = $searchParams.sort.sortField;
    switch ($searchParams.type) {
      case 'search':
        const searchingArray = $searchParams.filter === 'all' ? [...this.items] : this.filterItems($searchParams.filter);
        const searchString = $searchParams.searchString.toLowerCase();
        this.displayingItems =  searchingArray.filter((item: IItem) => item.title.toLowerCase().indexOf(searchString) !== -1 ||
          item.type.toLowerCase().indexOf(searchString) !== -1 || item.keyword.toLowerCase().indexOf(searchString) !== -1);
        break;
      case 'filter':
        this.displayingItems = $searchParams.filter === 'all' ? [...this.items] : this.filterItems($searchParams.filter);
        break;
      case 'clear':
        this.displayingItems = [...this.items];
        this.initSort();
        return;
    }
    this.displayingItems = this.sortItems(sortField, $searchParams);
  }

  ngOnInit() {
    this.items = this.dataService.itemsArray;
    this.displayingItems = [...this.items];
    this.dataService.$searchParams.subscribe((params: ISearch) => {
      this.searchItems(params);
    });
    this.initSort();
  }

}
