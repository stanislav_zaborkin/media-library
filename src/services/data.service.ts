import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ISearch } from '../interfaces/search';
import { IItem } from '../interfaces/item';
import { IOption } from '../interfaces/option';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  itemsArray: IItem[];
  selectCaption: {
    filter: string,
    sort: string
  };
  searchInit: ISearch = {
    searchString: '',
    sort: {
      sortField: 'dateUploaded',
      sortType: 1
    },
    type: '',
    filter: 'all',
    selectCaption: ''
  };
  $searchParams: BehaviorSubject<ISearch> = new BehaviorSubject(this.searchInit);
  selectOptions: {
    filter: IOption[],
    sort: IOption[]
  };
  isSelectVisible = new Subject();

  constructor() {
    this.itemsArray = [
      {
        title: 'Video File Sport [1]',
        type: 'video',
        keyword: 'sport',
        dateUploaded: 13005720000003
      },
      {
        title: 'Video File Nature [2]',
        type: 'video',
        keyword: 'nature',
        dateUploaded: 1241211600000
      },
      {
        title: 'Video File Science [3]',
        type: 'video',
        keyword: 'science',
        dateUploaded: 1241211600000
      },
      {
        title: 'Inter. Video File [1]',
        type: 'interactive_video',
        keyword: 'physical',
        dateUploaded: 1341211600000
      },
      {
        title: 'Inter. Video File [2]',
        type: 'interactive_video',
        keyword: 'sport',
        dateUploaded: 1301211600000
      },
      {
        title: 'Inter. Video File [3]',
        type: 'interactive_video',
        keyword: 'nature',
        dateUploaded: 1271211600000
      },

      {
        title: 'Audio File [1]',
        type: 'audio',
        keyword: 'rock',
        dateUploaded: 1264975200000
      },
      {
        title: 'Audio File [2]',
        type: 'audio',
        keyword: 'rap',
        dateUploaded: 915141600000
      },
      {
        title: 'Audio File [3]',
        type: 'audio',
        keyword: 'reggie',
        dateUploaded: 1079301600000
      },
      {
        title: 'Image File [1]',
        type: 'image',
        keyword: 'nature',
        dateUploaded: 1330120800000
      },
      {
        title: 'Image File [2]',
        type: 'image',
        keyword: 'mountains',
        dateUploaded: 1424124000000
      },
      {
        title: 'Image File [3]',
        type: 'image',
        keyword: 'sport',
        dateUploaded: 1328997600000
      },
      {
        title: 'Document File [1]',
        type: 'doc',
        keyword: 'sport',
        dateUploaded: 1496869200000
      },
      {
        title: 'Document File [2]',
        type: 'doc',
        keyword: 'nature',
        dateUploaded: 1515276000000
      },
      {
        title: 'Document File [3]',
        type: 'doc',
        keyword: 'science',
        dateUploaded: 1573077600000
      }
    ];
    this.selectOptions = {
      filter: [
        {
          title: 'All Media',
          value: 'all'
        },
        {
          title: 'Videos',
          value: 'video'
        },
        {
          title: 'Interactive videos',
          value: 'interactive_video'
        },
        {
          title: 'Audio',
          value: 'audio'
        },
        {
          title: 'Images',
          value: 'image'
        },
        {
          title: 'Documents',
          value: 'doc'
        }
      ],
      sort: [
        {
          title: 'Date Uploaded',
          value: 'dateUploaded'
        },
        {
          title: 'Alphabetical',
          value: 'title'
        }
      ]
    };
    this.selectCaption = {
      filter: 'Filter By:',
      sort: 'Sort By:'
    };
    this.isSelectVisible.next(false);
  }
}
