export interface IItem {
  title: string;
  type: string;
  keyword: string;
  dateUploaded: number;
}
