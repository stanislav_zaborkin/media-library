export interface ISearch {
  searchString: string;
  sort: {
    sortField: string;
    sortType: number;
  };
  type: string;
  filter: string;
  selectCaption: string;
}
